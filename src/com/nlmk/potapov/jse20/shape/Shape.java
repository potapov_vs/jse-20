package com.nlmk.potapov.jse20.shape;

public abstract class Shape {

    public abstract Double getArea();

}
