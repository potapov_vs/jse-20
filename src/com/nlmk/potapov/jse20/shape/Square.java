package com.nlmk.potapov.jse20.shape;

public class Square extends Shape{

    private Double sideSize;

    public Square(Double sideSize) {
        this.sideSize = sideSize;
    }

    public Double getSideSize() {
        return sideSize;
    }

    public void setSideSize(Double sideSize) {
        this.sideSize = sideSize;
    }

    @Override
    public Double getArea() {
        return Math.pow(sideSize,2);
    }
}
