package com.nlmk.potapov.jse20;

import com.nlmk.potapov.jse20.shape.Circle;
import com.nlmk.potapov.jse20.shape.Rectangle;
import com.nlmk.potapov.jse20.shape.Shape;
import com.nlmk.potapov.jse20.shape.Square;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Application {

    public static void main(String[] args) {
        final Collection<Circle> circles = new ArrayList<>();
        final Collection<Rectangle> rectangles = new ArrayList<>();
        final Collection<Square> square = new ArrayList<>();
        final Collection<Shape> shape = new ArrayList<>();

        addShape(circles, new Circle(5.0), new Circle(4.0));
        addShape(rectangles, new Rectangle(5.0, 2.0), new Rectangle(4.0, 2.0));
        addShape(square, new Square(5.0), new Square(4.0));
        addShape(shape, new Circle(5.0), new Rectangle(5.0, 2.0), new Square(5.0));


        System.out.println(getShapeCollectionArea(circles));
        System.out.println(getShapeCollectionArea(rectangles));
        System.out.println(getShapeCollectionArea(square));
        System.out.println(getShapeCollectionArea(shape));

    }

    private static <T extends Shape> void addShape(final Collection<T> shapeCollection, final T ... shapes) {
        shapeCollection.addAll(Arrays.asList(shapes));
    }

    private static <T extends Shape> Double getShapeCollectionArea(final Collection<T> shapeCollection) {
        Double area = 0.0;
        for (T shape: shapeCollection){
            area = area + shape.getArea();
        }
        return area;
    }

}
